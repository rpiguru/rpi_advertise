import os
from kivy.clock import Clock
from kivy.config import Config

Config.read(os.path.expanduser('~/.kivy/config.ini'))
Config.set('graphics', 'width', '1920')
Config.set('graphics', 'height', '1080')
Config.set('kivy', 'keyboard_mode', 'systemanddock')
Clock.max_iteration = 20
